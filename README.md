# Rails Easy Snippets Extension README

Ruby on Rails 6 snippets for embedded ruby files with `erb` extension.

## Download link
The Extension is available at https://marketplace.visualstudio.com/items?itemName=SamroodAli.erb

## Current Snippets

* Form_with for forms and other form components

<!-- \!\[feature X\]\(images/feature-x.png\) -->


## Release Notes

Users appreciate release notes as you update your extension.

### 1.0.0

Initial release of form snippets

### 1.4.2
New in 1.4.2:
Custom non database default forms snippets
erb and er= for templates to be placed on html
Released 

## Authors

![Samrood Ali's GitHub stats](https://github-readme-stats.vercel.app/api?username=SamroodAli&count_private=true&theme=dark&show_icons=true)

👤 **Samrood Ali**
- GitHub: [@githubhandle](https://github.com/SamroodAli)
- LinkedIn: [LinkedIn](https://www.linkedin.com/in/samrood-ali/)


